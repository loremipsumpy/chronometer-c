/*
 * File:   main.c
 * Author: Adrián Vera
 *
 * Created on September 25, 2016, 7:32 PM
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

//global variables
int globalValue;                                    // contador global
int debug = 1;                                      // 1 para activar debug en consola, 0 para flujo normal del programa
static volatile sig_atomic_t doneflag = 0;          // bandera de terminación

//function
int cronometro(unsigned int second);

void clearScreen();

void pauseConsole();

static void setDoneFlag(int signo);

/**
 * Cronometro, el programa espera que se le pase como primer argumentoun tiempo T
 * en segundos, y un valor numerico N como segundo parametro, el programa va a
 * registrar cada vez que transcurra los T segundos y aumentará un contador en 1,
 * una vez el el contador llegue al valor N ingresado como parametro, el programa
 * permitirá reniciar el contador o dar a finalizado el programa. Sí el programa
 * es interrumpudo, mostrará el valor del contador.
 * @param argc -> La cantidad de parametros que se le pasa al programa
 * @param argv -> Un array con los parametros envíados
 * @return (EXIT_SUCCESS && el valor del contador) || ( EXIT_FAILURE && la esepción del programa )
 */
int main(int argc, char *argv[]) {

    //------------------ Creación de variables -----------------------//
    struct sigaction act;
    unsigned int seconds;
    int limit;
    char answer;
    //------------------ Fin Creación de variables -------------------//

    //---------- Esepciones de inicio de programa --------------------//
    if (argc != 3) {
        fprintf(stderr, ";Usage:%s seconds limit\n", argv[0]);
        return (EXIT_FAILURE);
    }
    act.sa_handler = setDoneFlag;
    /* set up signal handler */
    act.sa_flags = 0;
    if ((sigemptyset(&act.sa_mask) == -1) ||
        (sigaction(SIGINT, &act, NULL) == -1)) {
        perror("Failed to set SIGINT handler");
        return (EXIT_FAILURE);
    }
    //---------- Fin Esepciones de inicio de programa-----------------//

    seconds = (unsigned int) atoi(argv[1]);
    limit = atoi(argv[2]);
    globalValue = 0;

    //---------- Simple codigo debug ---------------------------------//
    if (debug == 1) {
        fprintf(stderr, "Debug on");
        fprintf(stderr, "Tiempo del cronometro: %d\n", seconds);
        fprintf(stderr, "Limite: %d\n", limit);
        fprintf(stderr, "Press intro\n");
        pauseConsole();
    }
    //---------- Fin Simple codigo debug -----------------------------//

    while (!doneflag) {
        if (globalValue >= limit) {
            fprintf(stderr, "Limite alcanzado ¿empezar de nuevo? S, N\n");
            scanf(" %c", &answer);
            getchar();
            if (answer == 's') {
                globalValue = 0;
            } else {
                return (EXIT_SUCCESS);
            }
        }
        cronometro(seconds);
        if (!doneflag)
            globalValue++;
    }
    fprintf(stderr, "Ejecución interrumpida...\n");
    fprintf(stderr, "globalValue: %d\n", globalValue);
    return (EXIT_SUCCESS);
}

/**
 * funcion que corre un cronometro por "second" segundos, la funcion
 * devuelve 1 al terminar la ejecución
 * @param second -> la cantidad de segundos del cronometro
 * @return 1 al terminar la ejecución
 */
int cronometro(unsigned int seconds) {

    //------------------ Creación de variables -----------------------//
    int s = 0;
    int m = 0;
    int h = 0;
    int totalSecond = 0;
    //------------------ Fin Creación de variables -------------------//

    for (;;) {
        if (s >= 60) {
            s = 0;
            m++;
            if (m >= 60) {
                m = 0;
                h++;
            }
        }
        s++;
        totalSecond++;

        //---------- Simple codigo debug ---------------------------------//
        if (debug == 1) {
            clearScreen();
            fprintf(stderr, "%d:%d:%d     globalValue: %d   pid:%d\n", h, m, s, globalValue, getpid());
        }
        //---------- Fin Simple codigo debug -----------------------------//

        sleep(1);
        if (totalSecond >= seconds || doneflag) {
            if (debug == 1)
                fprintf(stderr, "End function cronometro\n");
            return (1);
        }
    }
}


/**
 * funcion para poder limpiar la pantalla tanto en windows
 * como en linux asumiendo posix como interprete de linux
 */
void clearScreen() {
#ifdef _WIN32
    system("cls");
#else
    // Assume POSIX
    system("clear");
#endif
}

/**
 * funcion para pausar la ejecución en la consola tanto en
 * windows como en linux asumiendo posix como interprete de linux
 */
void pauseConsole() {
#ifdef _WIN32
    system("pause");
#else
    getchar();
#endif
}

/**
 * funcion para agregar a la estructura
 * @param signo -> número del error, para más información
 * revisar <signum.h>
 */
static void setDoneFlag(int signo) {
    doneflag = 1;
}